import { SequenzaWebPage } from './app.po';

describe('sequenza-web App', () => {
  let page: SequenzaWebPage;

  beforeEach(() => {
    page = new SequenzaWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
