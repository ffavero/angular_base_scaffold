import { SeoPageIdGuard } from './seo-page-id-guard'
import { SeoGuard } from './seo.guard'
import { SeoService } from './seo.service'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule, Routes } from '@angular/router'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HttpClientModule } from '@angular/common/http'
import { FlexLayoutModule } from '@angular/flex-layout'
import { DisqusModule } from 'ngx-disqus'
import { NavComponent } from './nav/nav.component'
import { LayoutModule } from '@angular/cdk/layout'
import {DomSanitizer} from '@angular/platform-browser'
import { MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatTabsModule,
  MatListModule,
  MatIconModule,
  MatIconRegistry,
  MatGridListModule,
  MatCardModule } from '@angular/material'
import { ProjectService } from './service/project.service'
import { NewsService } from './service/news.service'
import { AppComponent } from './app.component'
import { PageComponent } from './page/page.component'
import { AboutComponent } from './about/about.component'
import { ContactComponent } from './contact/contact.component'
import { ProjectsComponent } from './projects/projects.component'

const appRoutes: Routes = [
  {
    path: 'home', component: PageComponent,
    canActivate: [SeoGuard],
    runGuardsAndResolvers: 'always',
    data: {
      title: ['Sequenza projects'],
      desc: 'List of Sequenza projects'
    }
  },
  {
    path: 'about', component: AboutComponent,
    canActivate: [SeoGuard],
    runGuardsAndResolvers: 'always',
    data: {
      title: ['About sequenza'],
      desc: 'News and citations of the project'
    }
  },
  {
    path: 'contact', component: ContactComponent,
    canActivate: [SeoGuard],
    runGuardsAndResolvers: 'always',
    data: {
      title: ['Contacts'],
      desc: 'Contacts and info'
    }
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    ProjectsComponent,
    NavComponent,
    ContactComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({
      appId: 'sequenza-web'
    }),
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    DisqusModule.forRoot('sequenza'),
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatTabsModule,
    MatListModule,
    MatGridListModule,
    MatCardModule
  ],
  providers: [
    MatIconRegistry, SeoService, SeoGuard,
    SeoPageIdGuard, ProjectService, NewsService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    )
    matIconRegistry.addSvgIcon(
      'sequenza',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/sequenzacolor.svg')
    )
    matIconRegistry.addSvgIcon(
      'sequenza-black',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/sequenzaalpha.svg')
    )
    matIconRegistry.addSvgIcon(
      'sequenza-python',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/sequenzapython.svg')
    )
    matIconRegistry.addSvgIcon(
      'rlogo',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/R_logo.svg')
    )
    matIconRegistry.addSvgIcon(
      'pylogo',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/pylogo.svg')
    )
    matIconRegistry.addSvgIcon(
      'html5logo',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/html5logo.svg')
    )
    matIconRegistry.addSvgIcon(
      'docker',
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/images/docker.svg')
    )
  }
}
