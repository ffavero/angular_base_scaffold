import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, throwError as observableThrowError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'

import { Project } from '../model/project'


@Injectable()
export class ProjectService {
  private projectsUrl = 'assets/projects.json'

  constructor(private http: HttpClient) {}

  getProjects() {
    return this.http
      .get<Project[]>(this.projectsUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }
  
  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }

}
