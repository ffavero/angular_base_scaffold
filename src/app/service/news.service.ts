import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, throwError as observableThrowError } from 'rxjs'
import { catchError, map } from 'rxjs/operators'

import { News } from '../model/news'


@Injectable()
export class NewsService {
  private newsUrl = 'assets/news.json'

  constructor(private http: HttpClient) {}

  getNews() {
    return this.http
      .get<News[]>(this.newsUrl)
      .pipe(
        map(data => {
          for (var k in data) {
            data[k].date = new Date(data[k].date)
          }
          data.sort(function(a, b){
            var keyA = a.date, keyB = b.date
            if(keyA < keyB) return -1
            if(keyA > keyB) return 1
            return 0
          })
          return(data.reverse())
        }),
        catchError(this.handleError)
      )
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }

}
