import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { News } from '../model/news'
import { NewsService } from '../service/news.service'

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})


export class AboutComponent implements OnInit {
  news: News[]
  error: any

  constructor(private router: Router, private newsService: NewsService) {}

  getNews(): void {
    this.newsService
      .getNews()
      .subscribe(
        news => (this.news = news),
        error => (this.error = error)
      )
  }

  ngOnInit(): void {
      this.getNews()
  }

}
