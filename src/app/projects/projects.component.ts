import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Project } from '../model/project'
import { ProjectService } from '../service/project.service'
import { ObservableMedia } from '@angular/flex-layout'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/takeWhile'
import 'rxjs/add/operator/startWith'

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Project[]
  error: any
  showNgFor = false

  constructor(private router: Router, private projectService: ProjectService,
    private observableMedia: ObservableMedia) {}

  getProjects(): void {
    this.projectService
      .getProjects()
      .subscribe(
        projects => (this.projects = projects),
        error => (this.error = error)
      )
  }

  public cols: Observable<number>;

  ngOnInit(): void {
      this.getProjects()
      const grid = new Map([
        ['xs', 1],
        ['sm', 2],
        ['md', 2],
        ['lg', 3],
        ['xl', 3]
      ]);
      let start: number
      grid.forEach((cols, mqAlias) => {
        if (this.observableMedia.isActive(mqAlias)) {
          start = cols
        }
      })
      this.cols = this.observableMedia.asObservable()
        .map(change => {
          return grid.get(change.mqAlias)
        })
        .startWith(start)
  }

}
