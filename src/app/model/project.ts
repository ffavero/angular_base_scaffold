export class Project {
  name: string
  description: string
  subtitle: string
  url: string
}
