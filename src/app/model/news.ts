export class News {
  date: Date
  title: string
  description: string
  link: string
}
